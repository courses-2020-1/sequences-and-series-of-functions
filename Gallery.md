```math
f_{n}\left(x\right)=x^{\frac{2n}{2n-1}}
```

[![$`f_{n}\left(x\right)`$](img/9.png)](https://www.desmos.com/calculator/vj6tpjxlkl "graph of ")

[![$`f_{n}\left(x\right)`$](https://gitlab.com/courses-2020-1/sequences-and-series-of-functions/-/jobs/artifacts/master/raw/src/julia/9.svg?job=plot)](https://gitlab.com/courses-2020-1/sequences-and-series-of-functions/-/jobs/artifacts/master/raw/src/julia/test.svg?job=plot "graph of ")
