# Sucesiones y series de funciones Capítulo $`7`$ ([Walter Rudin](https://en.wikipedia.org/wiki/Walter_Rudin))

En el presente capítulo limitaremos nuestra atención a las funciones
de variables complejas (en ellas incluiremos, naturalmente, las de
valores reales) aunque muchos de los teoremas y demostraciones que
siguen se amplian sin dificultad a las funciones vectoriales, e
incluso a los mapeos en espacios métricos en general.

Elegimos el trabajar en este marco reducido para fijar la atención
en los aspectos más importantes de los problemas que se presentan
cuando se varía el orden en los procesos de límites.

## Discusión del problema principal

### Definición

Supongamos que $`\left\{f_{n}\right\}`$, con $`n=1,2,3,\ldots`$, es
una sucesión de funciones definidas en un conjunto $`E`$ y que la
sucesión de números $`\left\{f_{n}\left(x\right)\right\}`$ converge
para todo $`x\in E`$.
Podemos definir una función $`f`$ por
```math
f\left(x\right)=\lim_{n\to\infty}f_{n}(x)\quad(x\in E)
```
En estas circunstancias, decimos que $`\left\{f_{n}\right\}`$
converge en $`E`$ y que $`f`$ es el *límite* o la *función límite*,
de $`\left\{f_{n}\right\}`$.
A veces, utilizaremos una terminología más expresiva y diremos que
"$`\left\{f_{n}\right\}`$ converge hacia $`f`$ *puntualmente* en
$`E`$", o bien, "en cada punto de $`E`$", si se cumple (1).
Del mismo modo, si $`\sum f_{n}\left(x\right)`$ converge para todo
$`x\in E`$ y definimos
```math
f\left(x\right)=
\sum_{n=1}^{\infty}f_{n}\left(x\right)\quad
(x \in E),
```
a la función $`f`$ se le llama *suma* de la serie $`\sum f_{n}`$.

El principal problema que se presenta es el de determinar qué
propiedades de las funciones se conservan con las operaciones de
límites (1) y (2).
Por ejemplo, si las funciones $`f_{n}`$ son continuas, o
diferenciables o integrables, ¿sucede lo mismo con la función límite?
¿cuáles son las relaciones entre $`f_{n}^{\prime}`$ y $`f^{\prime}`$
o entre las integrales de $`f_{n}`$ y la de $`f`$?
Decir que $`f`$ es continua en $`x`$ significa que
```math
\lim_{t\to x}f\left(t\right)=
f\left(x\right)
```
Por tanto, preguntar si el límite de una sucesión de funciones
continuas es continua, es lo mismo que preguntar si
```math
\lim_{t\to x}\lim_{n\to\infty}f_{n}\left(t\right)=
\lim_{n\to\infty}\lim_{t\to x}f_{n}\left(t\right)
```
esto es, si no importa el orden en que se aplica el proceso de
límites.
En el primer miembro de (3), hacemos primero $`n\to\infty`$, y luego,
$`t\to x`$; en el segundo, primero $`t\to x`$, y luego,
$`n\to\infty`$.

Vamos a demostrar, por medio de ejemplos que, en general, no se puede
variar el orden en los procesos de límites sin afectar al resultado.
Después demostraremos que, en ciertas condiciones, no tiene
importancia este orden.
El primer ejemplo, y el más sencillo, se refiere a una
[*sucesión doble*](https://encyclopediaofmath.org/wiki/Double_sequence).

### Ejemplo

Para $`m=1,2,3,\ldots`$ y $`n=1,2,3,\ldots`$ sea
```math
s_{m,n}=
\frac{m}{m+n}
```
Para todo $`n`$ prefijado, será
```math
\lim_{m\to\infty}s_{m,n}=
1
```
de modo que
```math
\lim_{n\to\infty}\lim_{m\to\infty}s_{m,n}=
1
```
Por otro lado, para todo $`m`$ prefijado
```math
\lim_{n\to\infty}s_{m,n}=
0
```
de modo que
```math
\lim_{m\to\infty}\lim_{n\to\infty}s_{m,n}=
0
```

### Ejemplo

Sea
```math
f_{n}\left(x\right)=
\frac{x^{2}}{\left(1+x^{2}\right)^{n}}\quad
(x\text { real };n=0,1,2,\ldots),
```
y consideremos
```math
f\left(x\right)=
\sum_{n=0}^{\infty}f_{n}\left(x\right)=
\sum_{n=0}^{\infty}\frac{x^{2}}{\left(1+x^{2}\right)^{n}}
```
Como $`f_{n}\left(0\right)=0`$, tenemos que $`f\left(0\right)=0`$.
Para $`x\neq0`$, la última serie de (6) es una serie geométrica
convergente con suma $`1+x^{2}`$ (Teorema 3.26 ).
Por tanto,
```math
f\left(x\right)=
\begin{cases}
0, & x=0 \\
1+x^{2}, & x\neq0
\end{cases}
```
de forma que una serie convergente de funciones continuas puede
tener una suma discontinua.

### Ejemplo

Para $`m=1,2,3,\ldots`$, hagamos
```math
f_{t}\left(x\right)=
\lim_{n\to\infty}(\cos m!\pi x)^{2n}
```
Cuando $`m!x`$ es entero, $`f_{m}\left(x\right)=1`$.
Para todo otro valor de $`x,f_{m}\left(x\right)=0`$.
Sea ahora
```math
f\left(x\right)=
\lim_{m\to\infty}f_{m}\left(x\right)
```
Para $`x`$ irracional, $`f_{m}\left(x\right)=0`$ para todo $`m`$, por lo
cual $`f\left(x\right)=0`$.
Para $`x`$ racional, es decir, $`x=\frac{p}{q}`$, siendo $`p`$ y
$`q`$ enteros, vemos que $`m!x`$ es entero si $`m\geq q`$, de modo
que $`f\left(x\right)=1`$.
Por tanto
```math
\lim_{m\to\infty}
\lim_{n\to\infty}{\left(\cos m!\pi x\right)}^{2n}=
\begin{cases}
0, & x\in\mathbb{I} \\
1, & x\in\mathbb{Q}
\end{cases}
```
Hemos obtenido, así, una función limite discontinua en todas partes
que no es integrable, según Riemann.

### Ejemplo

Sea
```math
f_{n}\left(x\right)=
\frac{\operatorname{sen}nx}{\sqrt{n}}\quad
(x\in\mathbb{R}, n=1,2,3,\ldots),
```
y
```math
f\left(x\right)=
\lim_{n\to\infty}f_{n}\left(x\right)=
0
```
Entonces, $`f^{\prime}\left(x\right)=0`$, y
```math
f_{n}^{\prime}\left(x\right)=
\sqrt{n}\cos nx
```
de modo que $`\left\{f_{n}^{\prime}\right\}`$ no converge hacia
$`f^{\prime}`$.
Por ejemplo,
```math
f_{n}^{\prime}\left(0\right)=
\sqrt{n}\to+\infty
```
cuando $`n\to\infty`$, mientras que $`f^{\prime}\left(0\right)=0`$.

### Ejemplo

Sea
```math
f_{n}\left(x\right)=
n^{2}x{\left(1-x^{2}\right)}^{n}\quad
(0\leq x\leq 1,n=1,2,3,\ldots).
```
Para $`0<x\leq1`$, tenemos
```math
\lim_{n\to\infty}f_{n}\left(x\right)=
0
```
por el Teorema 3.20(d).
Como $`f_{n}\left(0\right)=0`$, vemos que
```math
\lim_{n\to\infty}f_{n}\left(x\right)=
0\quad(0\leq x\leq1)
```
Un cálculo sencillo demuestra que
```math
\int_{0}^{1}x\left(1-x^{2}\right)^{n}dx=
\frac{1}{2n+2}
```
Asi, pues, a pesar de
```math
\int_{0}^{1}f_{n}\left(x\right)dx=
\frac{n^{2}}{2n+2}\to+\infty
```
cuando $`n\to\infty`$.
Si sustituimos en (10) $`n^{2}`$ por $`n`$, se cumple todavia (11),
pero, tenemos
```math
\lim_{n\to\infty}\int_{0}^{1}f_{n}\left(x\right)dx=
\lim_{n\to\infty}\frac{n}{2n+2}=
\frac{1}{2}
```
cuando
```math
\int_{0}^{1}
\left[\lim_{n\to\infty}f_{n}\left(x\right)\right]dx=
0.
```
Así, pues, el límite de la integral no es necesariamente igual a la
integral del límite, aun cuando los dos sean finitos.
Después de estos ejemplos que demuestran que podemos cometer un error
si se invierte el orden del proceso de límites descuidadamente,
definiremos una nueva forma de convergencia, más severa que la
puntual que aparecen la Definición 7.1, que nos permitirá llegar a
resultados positivos.

## Convergencia uniforme

### Definición

Decimos que una sucesión de funciones $`\left\{f_{n}\right\}`$ con
$`n=1,2,3,\ldots`$, converge uniformemente en $`E`$ hacia una función
$`f`$ si para cada $`\varepsilon>0`$ hay un entero $`N`$ tal que
$`n\geq N`$ implica
```math
\left|f_{n}\left(x\right)-f\left(x\right)\right|\leq
\varepsilon
```
para todo $`x\in E`$.
Es claro que toda sucesión uniformemente convergente es puntualmente
convergente.
Concretamente, la diferencia entre los dos conceptos es la siguiente:
si $`\left\{f_{n}\right\}`$ converge puntualmente en $`E`$, existe
una función $`f`$ tal que, para todo $`\varepsilon>0`$ y cada
$`x\in E`$, hay un entero $`N`$, que depende de $`\varepsilon`$ y de
$`x`$, tal que se cumple (12) si $`n\geq N`$; si
$`\left\{f_{n}\right\}`$ converge uniformemente en $`E`$, es posible
hallar, para cada $`\varepsilon>0`$ un entero $`N`$ tal que lo hace
para todo $`x\in E`$.

Decimos que la serie $`\sum f_{n}\left(x\right)`$ converge
uniformemente en $`E`$ si la sucesión $`\left\{s_{n}\right\}`$ de
sumas parciales definidas por
```math
\sum_{i=1}^{n}f_{i}\left(x\right)=
s_{n}\left(x\right)
```
converge uniformemente en $`E`$.
El criterio de Cauchy sobre convergencia uniforme es el siguiente:

### Teorema

La sucesión de funciones $`\left\{f_{n}\right\}`$, definida en $`E`$,
converge uniformemente en $`E`$ sii para cada $`\varepsilon>0`$
existe un entero $`N`$ tal que $`m\geq N`$, $`n\geq N`$, $`x\in E`$
implica
```math
\left|f_{n}\left(x\right)-f_{m}\left(x\right)\right|\leq
\varepsilon
```

#### Demostración

Supongamos que $`\left\{f_{n}\right\}`$ converge uniformemente en
$`E`$, y sea $`f`$ la función límite.
Entonces, existe un entero $`N`$ tal que $`n\geq N`$ y $`x\in E`$
implica
```math
\left|f_{n}\left(x\right)-f\left(x\right)\right|\leq
\frac{\varepsilon}{2},
```
de modo que
```math
\left|f_{n}\left(x\right)-f_{m}\left(x\right)\right|\leq
\left|f_{n}\left(x\right)-f\left(x\right)\right|+
\left|f\left(x\right)-f_{m}\left(x\right)\right|\leq
\varepsilon
```
si $`n\geq N,m\geq N`$, y $`x\in E`$.

Inversamente, supongamos que se cumple la condición de Cauchy.
Por el Teorema 3.11, la sucesión
$`\left\{f_{n}\left(x\right)\right\}`$
converge, para todo $`x`$, hacia un límite que podemos llamar
$`f\left(x\right)`$.
Así pues, la sucesión $`\forall_{n}`$ converge en $`E`$ hacia $`f`$.
Tenemos que demostrar que la convergencia es uniforme.

Sea $`\varepsilon>0`$ dado, y elijamos $`N`$ tal que se cumpla (13).
Fijemos $`n`$ y hagamos $`m\to\infty`$ en (13).
Como $`f_{m}\left(x\right)\to f\left(x\right)`$ cuando
$`m\to\infty`$, esto da
```math
\left|f_{n}\left(x\right)-f\left(x\right)\right|\leq
\varepsilon
```
para cada $`n\geq N`$ y todo $`x\in E`$, lo que completa la
demostración.
El criterio siguiente es útil algunas veces.

### Teorema

Supongamos
```math
\lim_{n\to\infty}f_{n}\left(x\right)=
f\left(x\right)\quad
\left(x\in E\right)
```
Hagamos
```math
M_{n}=
\sup_{x\in E}\left|f_{n}\left(x\right)-f\left(x\right)\right|
```
Entonces, $`f_{n}\to f`$ uniformemente en $`E`$ sii $`M_{n}\to0`$
cuando $`n\to\infty`$.

Como esto es consecuencia inmediata de la Definición 7.7, omitimos
los detalles de la demostración.

Hay un criterio de convergencia uniforme muy conveniente para las
series, debido a Weierstraß:

#### Teorema

Supongamos que $`\left\{f_{n}\right\}`$ es una sucesión de funciones
definidas en $`E`$, y supongamos
```math
\left|f_{n}\left(x\right)\right|\leq
M_{n}\quad
\left(x\in E,n=1,2,3,\ldots\right).
```
En estas condiciones, $`\sum f_{n}`$ converge uniformemente en $`E`$
si $`\sum M_{n}`$ converge.
Obsérvese que no se afirma la inversa (y de hecho, no es cierta).

#### Demostración

Si $`\sum M_{n}`$ converge, para $`\varepsilon>0`$ arbitrario,
```math
\left|\sum_{i=n}^{m}f_{i}\left(x\right)\right|\leq
\sum_{i=n}^{m}M_{i}\leq\varepsilon\quad(x\in E),
```
con tal que $`m`$ y $`n`$ sean suficientemente grandes.
La convergencia uniforme, se deduce del Teorema 7.8.
