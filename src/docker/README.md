# [![docker](https://www.docker.com/sites/default/files/d8/styles/role_icon/public/2019-07/Moby-logo.png)](https://www.docker.com)

## Installation

Please, follow the instructions for install Docker on your system.

[![Docker Beginner Tutorial 6 - How to install DOCKER on WINDOWS ? Step by Step](https://img.youtube.com/vi/ymlWt1MqURY/0.jpg)](https://www.youtube.com/embed/ymlWt1MqURY "Docker Beginner Tutorial 6 - How to install DOCKER on WINDOWS ? Step by Step")


## Build and push

```console
~/sequences-and-series-of-functions » docker login registry.gitlab.com -u username
~/sequences-and-series-of-functions » docker-compose build --pull
~/sequences-and-series-of-functions » docker-compose push
```

**login required only once time**