# Copyleft (c) August, 2020, Oromion.
FROM julia:1.5.1-buster

LABEL maintainer="Oromion <caznaranl@uni.pe>" \
	name="Plot" \
	description="Julia image with Gaston packages." \
	url="https://gitlab.com/courses-2020-1/sequences-and-series-of-functions/container_registry" \
	vcs-url="https://gitlab.com/courses-2020-1/sequences-and-series-of-functions" \
	vendor="Oromion Aznarán" \
	version="1.0"

ENV MAIN_PKGS="\
	git" \
	GIT_BUILD_PKGS="\
    make libssl-dev libghc-zlib-dev libcurl4-gnutls-dev libexpat1-dev gettext unzip" \
	GNUPLOT_PKGS="\
	gnuplot fonts-cmu" \
    DEBIAN_FRONTEND=noninteractive \
	JULIA_PROJECT=/root

COPY Manifest.toml $JULIA_PROJECT/Manifest.toml
COPY Project.toml $JULIA_PROJECT/Project.toml

RUN rm -f /etc/localtime && \
    ln -s /usr/share/zoneinfo/America/Lima /etc/localtime && \
    apt-get update -qq && \
    apt-get install -yq --no-install-recommends $MAIN_PKGS $GIT_BUILD_PKGS && \
	cd /tmp && curl -LO https://codeload.github.com/git/git/zip/v2.28.0 && \
	unzip v2.28.0 && cd git-2.28.0 && make prefix=/usr/local all && \
	make prefix=/usr/local install

RUN	julia -e 'using Pkg;Pkg.instantiate();using PackageCompiler;create_sysimage([:Gaston];cpu_target="generic",replace_default=true);'

RUN	apt-get install -yq --no-install-recommends $GNUPLOT_PKGS && \
	apt-get -yq purge $BUILD_PKGS && \
	apt-get -yq autoremove && \
    rm -rf /var/lib/apt/lists/*
