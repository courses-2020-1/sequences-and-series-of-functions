# $`\LaTeX`$

## 

[![arara](https://i.stack.imgur.com/hjUsN.png)](https://gitlab.com/islandoftex/arara)


`arara` is a TeX automation tool based on rules and directives.
It gives you a way to enhance your TeX experience.
The tool is an effort to provide a concise way to automate the daily
TeX workflow for users and also package writers.
Users might write their own rules when the provided ones do not suffice.


```console
~ » arara
  __ _ _ __ __ _ _ __ __ _
 / _` | '__/ _` | '__/ _` |
| (_| | | | (_| | | | (_| |
 \__,_|_|  \__,_|_|  \__,_|

Usage: arara [OPTIONS] [file]...

Options:
  -l, --log                        Generate a log output
  -v, --verbose / -s, --silent     Print the command output
  -n, --dry-run                    Go through all the motions of running a
                                   command, but with no actual calls
  -H, --header                     Extract directives only in the file header
  -t, --timeout INT                Set the execution timeout (in milliseconds)
  -L, --language TEXT              Set the application language
  -m, --max-loops INT              Set the maximum number of loops (> 0)
  -p, --preamble TEXT              Set the file preamble based on the
                                   configuration file
  -d, --working-directory DIRECTORY
                                   Set the working directory for all tools
  -V, --version                    Show the version and exit
  -h, --help                       Show this message and exit

Arguments:
  file  The file(s) to evaluate and process
```

[![arara showcase](https://i.imgur.com/uTlMcZK.png)](https://player.vimeo.com/video/279491730 "arara showcase - Click to Watch!")

### Basic use

To use `arara`, you need to tell it what to do. Unlike most other tools, you
give `arara` these _directives_ in the document itself – usually near the top.
So to run `pdflatex` once on your document, you should say something like:

```tex
% arara: pdflatex
\documentclass{article}
\begin{document}
Hello, world!
\end{document}
```

Now when you run `arara myfile`, that directive (`% arara: ...`) will be seen
and carried out as described by the `pdflatex` rule.  You can read more about
rules and directives in the user manual available in our
[releases](https://gitlab.com/islandoftex/arara/-/releases) section. In addition
to documenting all of the rules that come standard with `arara`, the manual
gives a detailed explanation of how `arara` works, as well as how to create and
use your own rules.

### Examples

[Here](https://github.com/four-leaves/LaTeX-and-Friends/tree/master/tex/arara_examples) you find some easy to learn examples.

### Particular case

Look the lines $`39-40`$ of [`.gitlab-ci.yml`](https://gitlab.com/courses-2020-1/sequences-and-series-of-functions/-/blob/master/.gitlab-ci.yml#L39-40). We are using

```console
cd $CI_PROJECT_DIR/src/TeX/monograph && arara main
cd $CI_PROJECT_DIR/src/TeX/beamer && arara [^configu,prea]*.tex
```

[Convert svg to pdf with inkscape](https://graphicdesign.stackexchange.com/a/56792/88629)
