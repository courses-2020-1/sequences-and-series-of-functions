#!/usr/bin/env julia

using Gaston
# https://stackoverflow.com/a/52334969
x = range(0, 1, length=2000);
f(x, n::Integer) = ifelse.(0 .< x .<= 1 / n, 3 * n + 1, 0);
A = Axes(title="'n=1,...,20'", linetype=:sunset);

plot(x, f(x, 1), lw=3, A);
for k = 2:20
    plot!(x, f(x, k), lw=3);
end

plot!(x[2:end], zeros(2000-1), linecolor="'blue'");

save(term="png", output=string(ARGS[1]) * string(".png"),
     saveopts="font 'CMU Serif,20' size 2048,1080");
