#!/usr/bin/env julia
using Gaston

t = range(-2, 2, length=1000);
f(t, σ) = exp.(-σ * abs.(t));
A = Axes(title=:Sucesión_de_funciones, linetype=:sunset);
plot(t, f(t, 0.5), lw=3, A);
plot!(t, f(t, 1), lw=3);
plot!(t, f(t, 1.5), lw=3);
plot!(t, f(t, 2), lw=3);
plot!(t, f(t, 2.5), lw=3);
plot!(t, f(t, 3), lw=3);
plot!(t, f(t, 3.5), lw=3);
plot!(t, f(t, 4), lw=3);
plot!(t, f(t, 4.5), lw=3);
plot!(t, f(t, 5), lw=3);
plot!(t, f(t, 5.5), lw=3);
plot!(t, f(t, 6), lw=3);

save(term="png", output=string(ARGS[1]) * string(".png"),
     saveopts="font 'CMU Serif,20' size 2048,1080");# lw 1 background 'blue'
