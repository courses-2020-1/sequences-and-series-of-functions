#!/usr/bin/env julia
using Gaston


x = range(0, 1, length=2000);
f(x, n::Integer) = max.(n .- n^2 .* abs.(x .- 1 / n), 0);
A = Axes(title="'n=2,...,20'", linetype=:sunset);

plot(x, f(x, 2), lw=3, A);
for k = 3:20
    plot!(x, f(x, k), lw=3);
end

plot!(x[2:end], zeros(2000 - 1), linecolor="'blue'");

save(term="png", output=string(ARGS[1]) * string(".png"),
     saveopts="font 'CMU Serif,20' size 2048,1080");
