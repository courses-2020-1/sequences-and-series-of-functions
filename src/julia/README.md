# [![julia](https://julialang.org/assets/infra/logo.svg)](https://julialang.org)

## Using Gaston

Firstly, download Julialang from [official sources](https://julialang.org/downloads).

Secondly, check a quick review of [Getting started](https://en.wikibooks.org/wiki/Introducing_Julia/Getting_started).

```console
$> julia start.jl 1
```

```jl
] add Gaston
julia> using Gaston
```

```jl
using PackageCompiler
create_sysimage(:Gaston, sysimage_path="gaston.so", precompile_execution_file="1.jl")
```

```jl
using Pkg
Pkg.activate("")
@time using Gaston
```

[Graphing functions with Julia](http://mth229.github.io/graphing.html)

[![Introduction to gnuplot(https://img.youtube.com/vi/OByVkOVqRl4/0.jpg)](https://www.youtube.com/embed/OByVkOVqRl4 "Introduction to gnuplot")

https://stackoverflow.com/a/56658333/9302545

<!-- https://www.springer.com/gp/book/9781441976826 -->
<!-- https://www.springer.com/gp/book/9781461403371 -->

https://www.arj.no/2012/02/07/latex-fonts
