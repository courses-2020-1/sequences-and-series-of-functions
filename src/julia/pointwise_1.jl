#!/usr/bin/env julia
using Gaston
# https://stackoverflow.com/questions/58681659/is-there-a-if-else-list-comprehension-in-julia
# https://stackoverflow.com/questions/54204198/using-julia-ifelse-with-an-array
# https://en.wikibooks.org/wiki/Introducing_Julia/Arrays_and_tuples
x = range(0, 1, length=2000);
f(x, n::Integer) = x.^(n);
A = Axes(title="'Convergencia puntual. n=1,...,20'", linetype=:sunset);

plot(x, f(x, 1), lw=3, A);

for k = 2:20
    plot!(x, f(x, k), lw=3);
end
plot!(x, ifelse.(0 .<= x .< 1, 0, 1), linecolor="'blue'");

save(term="png", output=string(ARGS[1]) * string(".png"),
     saveopts="font 'CMU Serif,20' size 2048,1080");
