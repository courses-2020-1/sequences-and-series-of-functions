#!/usr/bin/env julia
using Gaston

x = range(-1, 1, length=2000);
f(x, n::Integer) = ifelse.(abs.(x) .<= 1 / n, 1 / n, abs.(x));
A = Axes(title="'n=1,...,20'", linetype=:sunset);

plot(x, f(x, 1), lw=3, A);

for k = 2:20
    plot!(x, f(x, k), lw=3);
end

save(term="png", output=string(ARGS[1]) * string(".png"),
     saveopts="font 'CMU Serif,20' size 2048,1080");
