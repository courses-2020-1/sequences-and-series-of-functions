#!/usr/bin/env julia
using Gaston

x = range(0, 1, length=2000);
f(x, n::Integer) = ifelse.(float(2)^(-n - 1) .<= x .<= float(2)^(-n), 1 / n * sin.(float(2)^(n + 1) * pi *  x).^float(2), 0);
A = Axes(title="'n=1,...,20'", linetype=:sunset);
plot(x, f(x, 1), lw=3, A);

for k = 2:10
    plot!(x, f(x, k), lw=3);
end

save(term="png", output=string(ARGS[1]) * string(".png"),
     saveopts="font 'CMU Serif,20' size 2048,1080");
