#!/usr/bin/env julia
# https://stackoverflow.com/a/21057186/9302545
# https://stackoverflow.com/a/27827696/9302545
# https://bkamins.github.io/julialang/2020/05/10/julia-project-environments.html
# Usage: $> julia start.jl 1
if isfile("Project.toml") && isfile("Manifest.toml")
    using Pkg
    Pkg.activate(".")
end

include(string(ARGS[1]) * string(".jl"))
