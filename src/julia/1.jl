#!/usr/bin/env julia
using Gaston

t = 0:0.01:1;
plot(t, sin.(2π * 5 * t), linecolor=:coral, Axes(title=:First_plot));
plot!(
    t,
    cos.(2π * 5 * t),
    plotstyle="linespoints",
    pointtype="ecircle",
    linecolor="'blue'"
);

save(term="png", output=string(ARGS[1]) * string(".png"),
     saveopts="font 'CMU Serif,20' size 2048,1080");
