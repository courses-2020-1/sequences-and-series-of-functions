#!/usr/bin/env julia

using Gaston
# https://rosettacode.org/wiki/Nth_root#Julia
# https://stackoverflow.com/q/56164527/9302545
# Calculus Early Transcendentals	Volume:
# Author(s):	Howard Anton, Irl C. Bivens, Stephen Davis
# Graphing utililties sometimes omit portions of the graph of a function
# involving fractional exponents (or radicals).
# If f(x) = x^(p / q) , where p/q is a positive fraction in lowest terms,
# then you can circumvent this problem as follows:
# If p is even and q is odd, then graph g(x) = |x|^(p / q) instead of f(x) .
# If p is odd and q is odd, then graph g(x) = (|x|/x)|x| ^(p/q) instead of f(x).
x = range(-1, 1, length=2000);
f(x, n::Integer) = abs.(x).^((2*n)/(2*n-1));
A = Axes(title="'n=1,...,20'", linetype=:sunset);

plot(x, f(x, 3), lw=3, A);

for k = 2:20
    plot!(x, f(x, k), lw=3);
end

plot!(x[2:end], zeros(2000 - 1), linecolor="'blue'");

save(term="png", output=string(ARGS[1]) * string(".png"),
     saveopts="font 'CMU Serif,20' size 2048,1080");
