#!/usr/bin/env julia
using Gaston

t = 0:0.01:1;
plot(
      t,
      sin.(2π * 5 * t),
      with="linespoints",
      linecolor=:coral,
      Axes(title=:First_Plot, xtics="(0.25, 0.5, 0.75)")
);

save(term="png", output=string(ARGS[1]) * string(".png"),
     saveopts="font 'CMU Serif,20' size 2048,1080");
