#!/usr/bin/env julia
using Gaston

x = range(0, 1, length=2000);
f(x, n::Integer) = (n * exp.(x) - n^2 * exp.(-n .* x)) ./ (n^2 + 1);
A = Axes(title="'n=1,...,20'", linetype=:sunset);

plot(x, f(x, 1), lw=3, A);

for k = 2:10
    plot!(x, f(x, k), lw=3);
end

save(term="png", output=string(ARGS[1]) * string(".png"),
     saveopts="font 'CMU Serif,20' size 2048,1080");
