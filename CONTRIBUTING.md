## Contributing

```console
$> git clone https://gitlab.com/courses-2020-1/sequences-and-series-of-functions.git
$> cd sequences-and-series-of-functions
```

```console
$> git add --all
$> git commit -m "A descriptive comment"
$> git push origin master
```

For instance, for update the changes in your local computer:

```console
$> git pull origin master
remote: Enumerating objects: 10, done.
remote: Counting objects: 100% (10/10), done.
remote: Compressing objects: 100% (5/5), done.
remote: Total 6 (delta 3), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (6/6), 486 bytes | 27.00 KiB/s, done.
From gitlab.com:courses-2020-1/sequences-and-series-of-functions
 * branch            master     -> FETCH_HEAD
   b15963d..03c41db  master     -> origin/master
Updating b15963d..03c41db
Fast-forward
 src/TeX/monograph/abc.txt | 2 ++
 1 file changed, 2 insertions(+)
 create mode 100644 src/TeX/monograph/abc.txt
```

If git detects bifurcation of files, you need to specify which changes want to preserve, then do
```console
$> git merge origin master
```

Or in web browser as [here](https://docs.gitlab.com/ee/user/project/repository/web_editor.html).

## It will be right?

```console
$> git lfs uninstall
Hooks for this repository have been removed.
Global Git LFS configuration has been removed.
$> git rm .gitattribute
rm '.gitattributes'
$> rm -rf .git/lfs 
```

# Useful links
* [How to clear git cache](https://devconnected.com/how-to-clear-git-cache)
* [How to delete file on git](https://devconnected.com/how-to-delete-file-on-git)
* [Git filter repo](https://github.com/newren/git-filter-repo) for clean git history.
* [Reduce repository size](https://docs.gitlab.com/ee/user/project/repository/reducing_the_repo_size_using_git.html)

For a fresh cloning:

```console
$> git clone --depth=1 git@gitlab.com:courses-2020-1/sequences-and-series-of-functions.git
$> du -sh sequences-and-series-of-functions
1.4M    sequences-and-series-of-functions
```

https://github.com/James-Yu/LaTeX-Workshop/issues/376#issuecomment-398591211
