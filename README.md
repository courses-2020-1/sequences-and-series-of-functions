<div align="center">
<h1>
    Sequences and series of functions
</h1>
</div>

A project about the sequences and series of functions $`f\colon D\times\mathbb{N}\to Z,\quad\left(x,n\right)\mapsto f_{n}\left(x\right)`$.

* **Deadline** :timer_clock: $`9-11`$ september $`2️020`$.
* Net estimated monograph extension: $`11`$ pages (length).

<p align="center">
    <img src="https://gitlab.com/courses-2020-1/sequences-and-series-of-functions/-/jobs/artifacts/master/raw/src/julia/test.svg?job=plot" alt="Sequence of functions"/>
    <img src="https://gitlab.com/courses-2020-1/sequences-and-series-of-functions/-/jobs/artifacts/master/raw/src/TeX/img/graph0.svg?job=build_pdf" alt="Diagram of relations between properties related to sequence of functions"/>
</p>


## Schedule :calendar:

| Week             |                                     Activities                                     |                                 What will we do? |
| :--------------- | :--------------------------------------------------------------------------------: | -----------------------------------------------: |
| $`17-22/8/2020`$ |       **Assigned** by [`ahidalgog@uni.edu.pe`](mailto:ahidalgog@uni.edu.pe)        |                        Coordinate with partners. |
| $`24-28/8/2020`$ | Learn `git` and upload files for references, plot functions and explain main ideas |        Solve issues and publish the bibliography |
| $`31-4/9/2020`$  |                   Write in $`\LaTeX`$ the report and the slides                    | Explain to each partener the preliminary advance |
| $`7-11/9/2020`$  |              Do a nice exposition about the convergence of functions               |                                                  |

## Ideas :high_brightness:

### Week $`0`$

* Discusión del problema principal
* [Convergencia uniforme](https://de.wikipedia.org/wiki/Gleichm%C3%A4%C3%9Fige_Konvergenz)
* [Convergencia uniforme y continuidad]()
* [Convergencia uniforme e integración]()
* [Convergencia uniforme y diferenciación]()
* [Familia equicontinua de funciones]()
* [Teorema de Stone-Weierstraß](https://de.wikipedia.org/wiki/Satz_von_Stone-Weierstra%C3%9F)
* [Serie de potencias](https://de.wikipedia.org/wiki/Potenzreihe)
* [Función exponencial](https://de.wikipedia.org/wiki/Exponentialfunktion) y [logarítmica](https://de.wikipedia.org/wiki/Logarithmus)

### Week $`1`$
* https://webs.um.es/gvb/OCW/OCW-AM-II_files/LecAM-II.html
* Page 412  https://webs.um.es/gvb/OCW/OCW-AM-II_files/PDF/AM-II.pdf
* **Idea**: Is possible to generalize to uniform convergence with filter basis.

## References :books:

We dedicated and isoleted repository [sequences-and-series-of-functions-references](https://gitlab.com/courses-2020-1/sequences-and-series-of-functions-references), please checkout for contrast the written on [`beamer.pdf`](https://courses-2020-1.gitlab.io/sequences-and-series-of-functions/beamer.pdf) and [`main.pdf`](https://courses-2020-1.gitlab.io/sequences-and-series-of-functions/main.pdf). Thank you!

* [Walter Rudín Professor of Mathematics University of Wisconsin - Madison Principles of Mathematical Analysis Third Edition ](http://fulviofrisone.com/attachments/article/453/Principles%20Of%20Mathematical%20Analysis%20-%20W.Rudin.pdf)

* Chapter 7: Discussion of Main Problem
* Uniform Convergence
* Uniform Convergence and Continuity
* Uniform Convergence and Integration
* Uniform Convergence and Differenciation
* Equicontinuos Families of Functions
* The Stone-Weierstrass Theorem
* Exercises

* [Writing Proofs in Analysis](https://link.springer.com/book/10.1007/978-3-319-30967-5) 
* [Curso de Analise Vol 1 Elon Lages Lima]()
* [Análisis Matemático Vol 2 Haaser La Salle Sullivan]()
* [Holonomic function](https://en.wikipedia.org/wiki/Holonomic_function)
* [Fakultätenreihe](https://de.wikipedia.org/wiki/Fakult%C3%A4tenreihe)
* [Grenzwertkriterium](https://de.wikipedia.org/wiki/Grenzwertkriterium)
* [What are the applications of sequence of functions?](https://math.stackexchange.com/q/2592728)
* [Kompakte Konvergenz](https://de.wikipedia.org/wiki/Kompakte_Konvergenz)
* [Konvergente Mengenfolge](https://de.wikipedia.org/wiki/Konvergente_Mengenfolge)
* [Hausdorff-Konvergenz](https://de.wikipedia.org/wiki/Hausdorff-Konvergenz)
* [Konvergenzgeschwindigkeit](https://de.wikipedia.org/wiki/Konvergenzgeschwindigkeit)
* [Funktionenreihen](https://studyflix.de/mathematik/intro-funktionenreihen-904)
* [Funktionenfolgen und Konvergenz](http://www.math.uni-magdeburg.de/lehrver/analysis/gleichmaessige_konvergenz.pdf)
* [](https://www.math.uni-sb.de/ag/fuchs/HMI1_12_13/Begleitbuch/kap_8.pdf)
* [](https://www.informatik.uni-marburg.de/~geigert/teaching/sose13ft1/extra/konvergenzbegriffe.pdf)

We are using [Git Large File Storage](https://docs.gitlab.com/ee/topics/git/lfs/index.html) for save the `*.pdf`'s.
Then, if you need to save on your disk execute
```console
git lfs fetch origin master
```

## [GitLab Runner](https://docs.gitlab.com/runner/install) :runner:

[GitLab Runner](https://docs.gitlab.com/runner/) is the open source
project that is used to run your jobs and send the results back to
GitLab and can be installed and used on [GNU/Linux](https://www.gnu.org/gnu/linux-and-gnu.html),
macOS, FreeBSD, and Windows.

The best way is use [Docker](https://www.docker.com) :whale: or download a binary manually.
After that you need to go `https://gitlab.com/courses-2020-1/sequences-and-series-of-functions/-/settings/ci_cd`
(**login required**) and find the *Runner* section
![](img/runner.png)

or for a full concise explanation, please check [Raghav Pal](https://automationstepbystep.com).

[![GitLab Beginner Tutorial 6 | How to install GitLab Runner on Windows OS](https://img.youtube.com/vi/2MBhxk2chhM/0.jpg)](https://www.youtube.com/embed/2MBhxk2chhM "GitLab Beginner Tutorial 6 | How to install GitLab Runner on Windows OS")

Finally, if you already set the token succesfully you can run jobs with:
```console
gitlab-runner exec shell build_pdf
gitlab-runner exec docker build_pdf
gitlab-runner exec shell plot
gitlab-runner exec docker plot
```

## Artifacts :factory:

### To download :incoming_envelope:

* [`beamer.pdf`](https://gitlab.com/courses-2020-1/sequences-and-series-of-functions/-/jobs/artifacts/master/raw/src/TeX/beamer/beamer.pdf?job=build_pdf)
* [`main.pdf`](https://gitlab.com/courses-2020-1/sequences-and-series-of-functions/-/jobs/artifacts/master/raw/src/TeX/monograph/main.pdf?job=build_pdf)

### View document in web browser :globe_with_meridians:

* [`beamer.pdf`](https://courses-2020-1.gitlab.io/sequences-and-series-of-functions/beamer.pdf)
* [`main.pdf`](https://courses-2020-1.gitlab.io/sequences-and-series-of-functions/main.pdf)
